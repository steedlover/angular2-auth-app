'use strict';

var config = require("./config");

var gulp = require('gulp'),
  imagesMin = require('gulp-imagemin');

gulp.task('default', ['images'],
  function() { }
);

gulp.task('images', function() {
  return gulp.src(config.src_path + config.assets_path + config.img_path + '/**/*')
    .pipe(imagesMin({
      optimizationLevel: 5
     }))
    .pipe(gulp.dest(config.dest_path + config.assets_path + config.img_path));
});

