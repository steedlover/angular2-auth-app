var webpack             = require('webpack');
var path                = require('path');

var CopyWebpackPlugin   = require('copy-webpack-plugin');
var HtmlWebpackPlugin   = require('html-webpack-plugin');
//var LessPluginFunctions = require('less-plugin-functions');
var LessPluginCleanCSS  = require('less-plugin-clean-css');
//var LessPluginCssMedia  = require('group-css-media-queries');

var config = require("./config");

var htmlMinOptions = {
  empty: false,    // KEEP empty attributes
  cdata: true,     // KEEP CDATA from scripts
  comments: false, // KEEP comments
  dom: {
    /***
    * do not call .toLowerCase for each attribute
    * name (Angular2 use camelCase attributes)
    * ***/
    lowerCaseAttributeNames: false,
  }
};

// Webpack Config
var webpackConfig = {
  entry: {
    '_polyfills': config.src_path + '/polyfills.ts',
    '_vendor':    config.src_path + '/vendor.ts',
    'main':       config.src_path + '/app.ts',
    'styles':     [
      config.src_path + config.assets_path + config.css_path + '/styles.css'
      , config.src_path + config.assets_path + config.less_path + '/styles.less'
    ]
  },

  output: {
    path: config.dest_path
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: [
        'main',
        '_vendor',
        '_polyfills'
      ],
      minChunks: Infinity
    }),
    new HtmlWebpackPlugin({
      favicon: config.rel_src_path + config.assets_path + '/favicon.ico',
      inject: 'body',
      template: config.rel_src_path + '/index.html',
      chunksSortMode: 'none'
    }),
    new CopyWebpackPlugin([
      //{
        //from: 'src/assets/tmpl',
        //to: './assets/tmpl'
      //},
      {
        from: config.rel_src_path + config.assets_path + config.js_path,
        to:   config.root_assets_path + config.js_path
      }
    ])
  ],

  module: {
    loaders: [
      // .ts files for TypeScript
      { test: /\.ts$/, loader: 'awesome-typescript-loader' },
      // .less files for css styles
      {
        test: /\.less$/,
        exclude: /node_modules/,
        loader: "style!css!less?strictMath&noIeCompat&config=lessLoaderCustom"
      },
      // CSS loader
      {
        test: /\.css$/,
        exclude: /node_modules/,
        loader: "style-loader!css-loader"
      },
      // Fonts loader
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
        loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
      },
      // HTML minifier
      {
        test: /\.html$/,
        name: "mandrillTemplates",
        loader: 'raw!html-minify'
      }
    ]
  },
  'html-minify-loader': htmlMinOptions

};

// Our Webpack Defaults
var defaultConfig = {
  devtool: 'cheap-module-eval-source-map',
  cache: true,
  debug: true,
  output: {
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].map',
    chunkFilename: '[id].chunk.js'
  },

  module: {

    lessLoader: {
      lessPlugins: [
        //new LessPluginFunctions(),
        new LessPluginCleanCSS({advanced: true})
        //new LessPluginCssMedia()
      ]
    },

    preLoaders: [
      {
        test: /\.js$/,
        loader: 'source-map-loader',
        exclude: [
          // these packages have problems with their sourcemaps
          path.join(__dirname, 'node_modules', 'rxjs'),
          path.join(__dirname, 'node_modules', '@angular2-material'),
        ]
      }
    ],
    noParse: [
      path.join(__dirname, 'node_modules', 'zone.js', 'dist'),
      path.join(__dirname, 'node_modules', 'angular2', 'bundles')
    ]
  },

  resolve: {
    root: [ path.join(__dirname, 'src') ],
    extensions: ['', '.ts', '.js']
  },

  devServer: {
    historyApiFallback: true,
    watchOptions: { aggregateTimeout: 300, poll: 1000 }
  },

  node: {
    global: 1,
    crypto: 'empty',
    module: 0,
    Buffer: 0,
    clearImmediate: 0,
    setImmediate: 0
  },
}

var webpackMerge = require('webpack-merge');
module.exports = webpackMerge(
  defaultConfig
  , webpackConfig
);

