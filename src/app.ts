/// <reference path="./app/interfaces/users.d.ts" />
/// <reference path="./app/interfaces/firebase.d.ts" />
/// <reference path="./app/interfaces/moment.d.ts" />
/// <reference path="./app/interfaces/bootstrap.d.ts" />
/// <reference path="./app/interfaces/jquery.d.ts" />
/// <reference path="./app/interfaces/jquery.base64.d.ts" />

import {bootstrap} from '../node_modules/angular2/platform/browser';

import {RootElem} from './app/components/root_elem';
import {md5} from './app/services/md5';
import {FireBase} from './app/services/firebase';
import {Users} from './app/services/users';

bootstrap(RootElem, [
    md5
  , FireBase
  , Users
])
.catch(err => console.error(err));

