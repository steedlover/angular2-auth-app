"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('angular2/core');
var Observable_1 = require('rxjs/Observable');
var firebase_1 = require('./firebase');
var Users = (function () {
    function Users(fb) {
        var _this = this;
        this.fb = fb;
        this._users = null;
        this.users = new Observable_1.Observable(function (obsrver) {
            return _this._usersObserver = obsrver;
        });
        this.fb.getData('users', function (res) {
            _this._users = res;
            _this._usersObserver.next(_this._users);
        });
    }
    Users.prototype.getUsers = function () {
        return this._users;
    };
    Users.prototype.setUsers = function (users) {
        //this.fb.set(users);
    };
    Users = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(firebase_1.FireBase))
    ], Users);
    return Users;
}());
exports.Users = Users;
