import {Injectable} from '../../../node_modules/angular2/core';
import {Md5} from '../../../node_modules/ts-md5/dist/md5';

@Injectable()

export class md5 {

  getSimpleHash(str: string) {
    return Md5.hashStr(str);
  }

}

