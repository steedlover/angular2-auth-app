import {Injectable, Inject} from '../../../node_modules/angular2/core';
import {Observable} from '../../../node_modules/rxjs/Observable';
import {Observer} from '../../../node_modules/rxjs/Observer';
import {FireBase} from './firebase';
import {Cookie} from '../../../node_modules/ng2-cookies/ng2-cookies';
import {CookiesConf} from '../config/cookies';
// Import extra operators
import '../../../node_modules/rxjs/add/operator/share';

@Injectable()

export class Users {

  private users: IUser[] = null;

  public curUser: Observable<IUser>;
  private _curUserObserver: Observer<IUser>;
  private _curUser: IUser = null;

  public logOpened: Observable<boolean>;
  private _logOpenedObserver: Observer<boolean>;
  private _logOpened: boolean = true;

  private ckPref: string;
  private ckLoginName: string;
  private ckPasswName: string;
  private cookieName: string;
  private cookiePassw: string;
  private err: string = '';
  private loading: boolean = false;

  constructor(@Inject(FireBase) private fb) {

    this.ckPref      = CookiesConf.prefix;
    this.ckLoginName = this.ckPref + '_name';
    this.ckPasswName = this.ckPref + '_passw';

    this.cookieName  = Cookie.getCookie(this.ckLoginName);
    this.cookiePassw = Cookie.getCookie(this.ckPasswName);

    this.curUser = new Observable(observer =>
      this._curUserObserver = observer).share();
      // share() allows multiple subscribers

    this.logOpened = new Observable(observer =>
      this._logOpenedObserver = observer).share();

    this.fetchUsers();

  }

  private fetchUsers() {
    return this.fb.getData('users', (res: IUser[]) => {
      if(typeof(res) === 'object' && res) {
        this.users = res;
        /***
         * Fire up the cookies reading
         * when users array will be ready
         * ***/
        if(this.cookieName !== '' && this.cookiePassw !== ''
           && this.cookieName !== null && this.cookiePassw !== null)
          this.checkLogin(this.cookieName, this.cookiePassw);
        else
          this.openLogin();
      } else {
        this.users = [];
        this.openLogin();
      }
    });
  }

  public getUsers() {
    return this.users;
  }

  openLogin(newVal?: boolean) {
    newVal = typeof(newVal) === 'undefined' ? true : newVal;
    this._logOpened = newVal;
    this._logOpenedObserver.next(this._logOpened);
    return;
  }

  public checkLogin(username: string, userpassw: string, cb?: Function) {
    for(var u of this.users) {
      if(u.login === username) {
        if(u.passw === userpassw) {
          this._curUser = u;
          return setTimeout(() => {
            this._curUserObserver.next(this._curUser);
            // set cookies
            Cookie.setCookie(this.ckLoginName, u.login, CookiesConf.period);
            Cookie.setCookie(this.ckPasswName, u.passw, CookiesConf.period);
            this.cookieName = u.login;
            this.cookiePassw = u.passw;
            this.openLogin(false);
          }, 2000);
          //return this._curUserObserver.next(this._curUser);
        }
      }
    }

    return setTimeout(() => cb('Wrong pair login/password'), 2000);
    //return cb('Wrong pair login/password');
  }

  public doLogout() {
    this._curUser = null;
    this._curUserObserver.next(this._curUser);
    Cookie.deleteCookie(this.ckLoginName);
    Cookie.deleteCookie(this.ckPasswName);
    this.openLogin();
  }

  public setUsers(users: IUser[]) {
    //this.fb.set(users);
  }

}

