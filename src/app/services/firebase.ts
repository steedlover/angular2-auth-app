import {Injectable} from '../../../node_modules/angular2/core';
import {fbName} from '../config/firebase';

@Injectable()

export class FireBase {

  private fb: Firebase;

  constructor() {
    this.fb = new Firebase(fbName);
  }

  getData(tablename: string, cb?: Function) {
    this.fb
      .child(tablename)
      .on('value', (snapshot: FirebaseDataSnapshot) => {
        return cb ? cb(snapshot.val()) : null;
      });
  }

}

