interface IUser {
  id: string; // base64('created_time + login')
  login: string;
  name: string;
  passw: string; // md5('password')
  date_created: string; // YYYY-M-D H:m:s
}

