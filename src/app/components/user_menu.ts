import {Component
  , Inject
} from '../../../node_modules/angular2/core';
import {Users} from '../services/users';

@Component({
  selector: 'user-menu',
  templateUrl: '/assets/tmpl/user_menu.tpl',
  inputs: ['user']
})

export class UserMenu {

  private user: IUser;

  constructor(@Inject(Users) private usr) { }

  logout() {
    this.usr.doLogout();
  }

}

