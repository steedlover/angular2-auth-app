import {Component, Inject} from '../../../node_modules/angular2/core';
import {Users} from '../services/users';
import {TimeAgoPipe} from '../../../node_modules/angular2-moment';

declare var moment: moment.MomentStatic;

@Component({
  selector: 'users-list',
  pipes: [TimeAgoPipe],
  templateUrl: '/assets/tmpl/users_list.tpl'
})

export class UsersList {

  public users: IUser[];

  constructor(@Inject(Users) private usr
    //, @Inject(md5) private md5
    //, @Inject(FireBase) private fb
  ) {
      //this.base64enc = $.base64.encode(moment()
                           //.format('YYYY-MM-DD h:m:s a'));
      //this.base64dec = $.base64.decode(this.base64enc);
      //console.log(this.base64enc, this.base64dec);
      //setTimeout(() => {
        //this.yeah = md5.getSimpleHash('Hello world!');
      //}, 2000);

      if(typeof(this.users) === 'undefined' || !this.users) {
        this.users = usr.getUsers();
      }

  }

}

