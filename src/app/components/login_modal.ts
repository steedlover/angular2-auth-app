import {Component
  , Inject
  , EventEmitter
} from '../../../node_modules/angular2/core';
import {FORM_DIRECTIVES} from '../../../node_modules/angular2/common';
import {Users} from '../services/users';
import {md5} from '../services/md5';

export interface ILogin {
  login: string;
  passw: string;
}

@Component({
  selector: 'login-modal',
  templateUrl: '/assets/tmpl/login_modal.tpl',
  //inputs: ['opened'],
  //outputs: ['statusChange: openedChange'],
  directives: [FORM_DIRECTIVES]
})

export class LoginModal {

  private opened: boolean;
  private lg: ILogin;
  //private statusChange: EventEmitter<any> = new EventEmitter();
  private err: string = '';
  private loading: boolean = false;
  private modalVis: boolean = false;

  constructor(@Inject(Users) private usrServ,
              @Inject(md5) private md5) {

    // Subscribe to curUser global variable
    usrServ.curUser.subscribe(newVal => {
      if(newVal !== null) {
        this.opened = false;
        this.err = '';
        this.loading = false;
      }
    });
    usrServ.logOpened.subscribe(newVal => {
      if(typeof(newVal) !== 'undefined') {
        // Without timeout animation
        // of modal window is not working
        setTimeout(() => this.opened = newVal);
        this.getModalVis(newVal);
      }
    });
  }

  getModalVis(newVal: boolean) {
    if(newVal === true)
      this.modalVis = true;
    else
      setTimeout(() => {
        this.modalVis = false;
      }, 400);
    return;
  }

  //toggleStatus() {
    //this.opened = !this.opened;
  //}

  onSubmit(res: ILogin):void {
    this.loading = true;
    this.err = '';
    this.usrServ.checkLogin(res.login,
      this.md5.getSimpleHash(res.passw),
      (response: string) => {
        this.err = response;
        this.loading = false;
      });
  }

}

