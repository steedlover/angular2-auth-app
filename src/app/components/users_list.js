"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('angular2/core');
var md5_1 = require('../services/md5');
var firebase_1 = require('../services/firebase');
var users_1 = require('../services/users');
var UsersList = (function () {
    function UsersList(md5, fb, usr) {
        var _this = this;
        this.md5 = md5;
        this.fb = fb;
        this.usr = usr;
        //setTimeout(() => {
        //this.yeah = md5.getSimpleHash('Hello world!');
        //}, 2000);
        usr.users.subscribe(function (newVal) { return _this.users = newVal; });
    }
    UsersList = __decorate([
        core_1.Component({
            selector: 'users-list',
            templateUrl: '/assets/tmpl/users_list.tpl'
        }),
        __param(0, core_1.Inject(md5_1.md5)),
        __param(1, core_1.Inject(firebase_1.FireBase)),
        __param(2, core_1.Inject(users_1.Users))
    ], UsersList);
    return UsersList;
}());
exports.UsersList = UsersList;
