import {Component
  , Inject
  //, enableProdMode
} from '../../../node_modules/angular2/core';
import {Users} from '../services/users';
import {UsersList} from './users_list';
import {UserMenu} from './user_menu';
import {LoginModal} from '../components/login_modal';

//enableProdMode();

@Component({
  selector: 'root-elem',
  directives: [UsersList, UserMenu, LoginModal],
  templateUrl: '/assets/tmpl/root.tpl'
})

export class RootElem {

  private login: boolean = false;
  private current_user: IUser = null;

  constructor(@Inject(Users) private usr) {
    usr.curUser.subscribe(newVal => {
      this.current_user = newVal ? newVal : null;
    });
    usr.logOpened.subscribe(newVal => {
      this.login = typeof(newVal) === 'boolean'
        && newVal === true ? false : true;
    });
  }

}

