<div class="menu_wrapper">
  <div class="menu_wrapper_top">
    <h3 class="menu_wrapper_top_title">Users list</h3></div>
  <img src="/assets/img/cargando.gif" width="64" height="64"
    alt="Loading..." class="loading_img" *ngIf="!users" />
  <ul class="nav nav-pills nav-stacked nav_users" *ngIf="users?.length > 0">
    <li *ngFor="#user of users; #i = index"><a href="#"
      title="{{ user.name }}">#{{ i }}&nbsp;{{ user.name }}</a>
    <!--<span>Created {{ user.date_created | amTimeAgo }}</span>-->
    </li>
  </ul>
  <p *ngIf="users?.length === 0">No users...</p>
</div>

