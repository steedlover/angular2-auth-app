<!-- The Modal -->
<div id="login_modal" class="modal"
  [ngClass]="{'opened': modalVis, 'closed': !modalVis}">
  <!-- Modal content -->
  <div class="modal-content" [ngClass]="{'opened': opened}">
    <!--<span class="close" (click)="toggleStatus()">x</span>-->
    <h3>Please login to system</h3>
    <form #loginForm="ngForm" (ngSubmit)="onSubmit(loginForm.value)">
      <fieldset class="form-group">
        <div class="input-group">
          <span id="login-label" class="input-group-addon login_label_img">
          </span>
          <input type="text" class="form-control" placeholder="Input login"
            aria-describedby="login-label" maxlength="20" required
            ([ngModel])="lg.login" ngControl="login" #login="ngForm" />
        </div>
        <div class="label_error"
          [hidden]="login.valid || login.pristine">Login is required</div>
      </fieldset>
      <fieldset class="form-group">
        <div class="input-group">
          <span id="passw-label" class="input-group-addon passw_label_img">
          </span>
          <input type="password" class="form-control"
            placeholder="Input password" maxlength="30"
            aria-describedby="passw-label" required ngControl="passw" #passw="ngForm" ([ngModel])="lg.passw" />
        </div>
        <div class="label_error"
          [hidden]="passw.valid || passw.pristine">Password is required</div>
      </fieldset>
      <fieldset class="form-group">
        <input type="submit" class="btn btn-default"
          value="Login" [disabled]="!loginForm.form.valid" />
        <img src="/assets/img/cargando.gif" width="32" height="32"
          alt="Loading..." *ngIf="loading" />
        <span [hidden]="err === ''" class="label_error">{{ err }}</span>
      </fieldset>
    </form>

  </div>
</div>
<!--<pre>{{ opened | json }}</pre>-->

