<login-modal></login-modal>

<user-menu [user]="current_user"
  *ngIf="login && current_user !== null"></user-menu>
<div class="container" *ngIf="login">
  <div class="col-md-12 col-lg-4">
    <users-list></users-list>
  </div>
</div>

<div class="container container_dummy"
  *ngIf="!login && current_user === null"></div>

<img src="/assets/img/cargando.gif" width="64" height="64"
  class="loading_main_img" alt="Loading..."
  *ngIf="!login && current_user === null" />

