/// <reference path="./app/interfaces/users.d.ts" />
/// <reference path="./app/interfaces/firebase.d.ts" />
/// <reference path="./app/interfaces/users.d.ts" />
"use strict";
var browser_1 = require('angular2/platform/browser');
var root_elem_1 = require('./app/components/root_elem');
var md5_1 = require('./app/services/md5');
var firebase_1 = require('./app/services/firebase');
var users_1 = require('./app/services/users');
browser_1.bootstrap(root_elem_1.RootElem, [
    md5_1.md5,
    firebase_1.FireBase,
    users_1.Users
])
    .catch(function (err) { return console.error(err); });
