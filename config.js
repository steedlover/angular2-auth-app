/*** Main webconf config
 * variables ***/

module.exports = {
  rel_src_path          : "src",
  src_path              : "./src",
  dest_path             : "./build",
  assets_path           : "/assets",
  root_assets_path      : "./assets",
  less_path             : "/less",
  tmpl_path             : "/tmpl",
  img_path              : "/img",
  css_path              : "/css",
  js_path               : "/js",
  tmpl_ext              : ".tpl"
};
