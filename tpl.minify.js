'use strict';

var htmlMinifierOptions = {
  collapseInlineTagWhitespace: true,
  removeComments: true,
  collapseWhitespace: true,
  caseSensitive: true
};

var config = require("./config");

/***
 * Minifying templates files
 * and copy them to dist dir
 * ***/
var glob = require('webpack-glob-entries');
var minify = require('html-minifier').minify;
var fs = require('fs');
var mkdirp = require('mkdirp');
var templates = glob(config.src_path + config.assets_path + '/' + config.tmpl_path + "/**/*" + config.tmpl_ext);

var _keys = Object.keys(templates);
var _path = config.dest_path + config.assets_path + config.tmpl_path + '/';

_keys.forEach(function(key) {
  var fullpath = templates[key];
  var filename = key + config.tmpl_ext;
  var minifiedContent = minify(fs.readFileSync(fullpath, 'utf-8'),
                                htmlMinifierOptions);

  fs.access(_path, fs.F_OK, function(err) {
    mkdirp(_path, function(err) {
      if(!err) {
        fs.writeFile(_path + filename, minifiedContent, function(err) {
          if(err)
            console.error(err);
          else
            console.log('[' + fullpath + '] minified...');
        });
      }
    });
  });

});

